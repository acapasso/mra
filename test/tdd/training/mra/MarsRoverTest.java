package tdd.training.mra;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;



public class MarsRoverTest {

	@Test
	//User story 1
	public void CreatePlanet() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(4,7)");
		planetObstacles.add("(2,3)");
		MarsRover rover = new MarsRover(10,10,planetObstacles);
		assertEquals(true,rover.planetContainsObstacleAt(4,7));
		assertEquals(true,rover.planetContainsObstacleAt(2,3));
	}
	
	@Test
	//User story 2
	public void Landing() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(4,7)");
		planetObstacles.add("(2,3)");
		MarsRover rover = new MarsRover(10,10,planetObstacles);
		assertEquals("(0,0,N)",rover.executeCommand(""));
	}
	@Test
	//User story 3
	public void Turning() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(4,7)");
		planetObstacles.add("(2,3)");
		MarsRover rover = new MarsRover(10,10,planetObstacles);
		assertEquals("(0,0,E)",rover.executeCommand("r"));
	}
	
	@Test
	//User story 4
	public void MovingForward() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(4,7)");
		planetObstacles.add("(2,3)");
		MarsRover rover = new MarsRover(10,10,planetObstacles);
		assertEquals("(0,1,N)",rover.executeCommand("f"));
	}
	
	@Test
	//User story 5
		public void MovingBackward() throws MarsRoverException {
			List<String> planetObstacles = new ArrayList<>();
			planetObstacles.add("(4,7)");
			planetObstacles.add("(2,3)");
			MarsRover rover = new MarsRover(10,10,planetObstacles);
			rover.setLandingstatus("(5,8,E)");
			assertEquals("(4,8,E)",rover.executeCommand("b"));
		}
	@Test
	//User story 6
		public void MovingCombined() throws MarsRoverException {
			List<String> planetObstacles = new ArrayList<>();
			planetObstacles.add("(4,7)");
			planetObstacles.add("(2,3)");
			MarsRover rover = new MarsRover(10,10,planetObstacles);
			assertEquals("(2,2,E)",rover.executeCommand("ffrff"));
		}
	@Test
	//User story 7
		public void Wrapping() throws MarsRoverException {
			List<String> planetObstacles = new ArrayList<>();
			planetObstacles.add("(4,7)");
			planetObstacles.add("(2,3)");
			MarsRover rover = new MarsRover(10,10,planetObstacles);
			assertEquals("(0,9,N)",rover.executeCommand("b"));
		}

	@Test
	//User story 8
		public void SingleObstacles() throws MarsRoverException {
			List<String> planetObstacles = new ArrayList<>();
			planetObstacles.add("(2,2)");
			MarsRover rover = new MarsRover(10,10,planetObstacles);
			assertEquals("(1,2,E)(2,2)",rover.executeCommand("ffrfff"));
		}
	
	@Test
	//User story 9
		public void MultipleObstacles() throws MarsRoverException {
			List<String> planetObstacles = new ArrayList<>();
			planetObstacles.add("(2,2)");
			planetObstacles.add("(2,1)");
			MarsRover rover = new MarsRover(10,10,planetObstacles);
			assertEquals("(1,1,E)(2,2)(2,1)",rover.executeCommand("ffrfffrflf"));
		}
	@Test
	//User story 10
		public void WrappingandObstacles() throws MarsRoverException {
			List<String> planetObstacles = new ArrayList<>();
			planetObstacles.add("(0,9)");
			MarsRover rover = new MarsRover(10,10,planetObstacles);
			assertEquals("(0,0,N)(0,9)",rover.executeCommand("b"));
		}
	@Test
	//User story 11
		public void TourAroundAPlanet() throws MarsRoverException {
			List<String> planetObstacles = new ArrayList<>();
			planetObstacles.add("(2,2)");
			planetObstacles.add("(0,5)");
			planetObstacles.add("(5,0)");
			MarsRover rover = new MarsRover(6,6,planetObstacles);
			assertEquals("(0,0,N)(2,2)(0,5)(5,0)",rover.executeCommand("ffrfffrbbblllfrfrbbl"));
		}
	
	
}
