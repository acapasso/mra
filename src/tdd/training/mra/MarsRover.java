package tdd.training.mra;

import java.util.ArrayList;
import java.util.List;

public class MarsRover {

	/**
	 * It initializes the rover at the coordinates (0,0), facing North, on a planet
	 * (represented as a grid with x and y coordinates) containing obstacles.
	 * 
	 * @param planetX         The x dimension of the planet.
	 * @param planetY         The y dimension of the planet.
	 * @param planetObstacles The obstacles on the planet. Each obstacle is a string
	 *                        (without white spaces) formatted as follows:
	 *                        "(oi_x,oi_y)". <code>null</code> if the planet does
	 *                        not contain obstacles.
	 * 
	 * @throws MarsRoverException
	 */
	private int planetX;
	private int planetY;
	private List<String> planetObstacles;
	private String landingstatus;
	public MarsRover(int planetX, int planetY, List<String> planetObstacles) throws MarsRoverException {
		this.planetX=planetX;
		this.planetY=planetY;
		this.planetObstacles=planetObstacles;
		this.landingstatus="(0,0,N)";
	}

	
	public String getLandingstatus() {
		return landingstatus;
	}


	public void setLandingstatus(String landingstatus) {
		this.landingstatus = landingstatus;
	}


	/**
	 * It returns whether, or not, the planet (where the rover moves) contains an
	 * obstacle in a cell.
	 * 
	 * @param x The x coordinate of the cell
	 * @param y The y coordinate of the cell
	 * @return <true> if the cell contains an obstacle, <false> otherwise.
	 * @throws MarsRoverException
	 */
	public boolean planetContainsObstacleAt(int x, int y) throws MarsRoverException {
		boolean found = false;
		for(String s:this.planetObstacles)
		{
			int positionX=Integer.parseInt(s.substring(1,2));
			int positionY=Integer.parseInt(s.substring(3,4));
			if((positionX==x && positionY==y))
				found=true;
		}
		return found;
	}

	/**
	 * It lets the rover move on the planet according to a command string. The
	 * return string contains the new position of the rover, its direction, and the
	 * obstacles it has encountered while moving on the planet (if any).
	 * 
	 * @param commandString A string that can contain a single command -- i.e. "f"
	 *                      (forward), "b" (backward), "l" (left), or "r" (right) --
	 *                      or a combination of single commands.
	 * @return The return string that contains the position and direction of the
	 *         rover, and the obstacles the rover has encountered while moving on
	 *         the planet (if any). The return string (without white spaces) has the
	 *         following format: "(x,y,dir)(o1_x,o1_y)(o2_x,o2_y)...(on_x,on_y)". x
	 *         and y define the new position of the rover while dir represents its
	 *         direction (i.e., N, S, W, or E). Finally, oi_x and oi_y are the
	 *         coordinates of the i-th encountered obstacle.
	 * @throws MarsRoverException
	 */
	public String executeCommand(String commandString) throws MarsRoverException {
        int currentX = Integer.parseInt(this.landingstatus.substring(1, 2));
        int currentY = Integer.parseInt(this.landingstatus.substring(3, 4));
        char dir = this.landingstatus.charAt(5);
        String obstacles ="";
        int previousX;
        int previousY;

        for (char c : commandString.toCharArray()){
            switch (c){
                case ' ':
                    continue;
                case 'r':
                    dir = turnRight(dir);
                    continue;
                case 'l':
                    dir = turnLeft(dir);
                    continue;
                case 'f':
                    switch (dir){
                        case 'N':
                            previousX = currentX;
                            previousY = currentY;
                            if(currentY == planetY-1)
                                currentY = 0;
                            else
                                currentY++;
                            if(planetContainsObstacleAt(currentX, currentY)){
                            	if(!obstacles.contains("("+ Integer.toString(currentX)+","+Integer.toString(currentY)+")"))
                                    obstacles=obstacles+("("+ Integer.toString(currentX)+","+Integer.toString(currentY)+")");
                                currentX = previousX;
                                currentY = previousY;
                                continue;
                            }
                            continue;
                        case'S':
                            previousX = currentX;
                            previousY = currentY;
                            if(currentY == 0)
                                currentY = planetY-1;
                            else
                                currentY--;
                            if(planetContainsObstacleAt(currentX, currentY)){
                            	if(!obstacles.contains("("+ Integer.toString(currentX)+","+Integer.toString(currentY)+")"))
                                    obstacles=obstacles+("("+ Integer.toString(currentX)+","+Integer.toString(currentY)+")");
                                currentX = previousX;
                                currentY = previousY;
                                continue;
                            }
                            continue;
                        case'E':
                            previousX = currentX;
                            previousY = currentY;
                            if(currentX == planetX-1)
                                currentX = 0;
                            else
                                currentX++;
                            if(planetContainsObstacleAt(currentX, currentY)){
                            	if(!obstacles.contains("("+ Integer.toString(currentX)+","+Integer.toString(currentY)+")"))
                                    obstacles=obstacles+("("+ Integer.toString(currentX)+","+Integer.toString(currentY)+")");
                                currentX = previousX;
                                currentY = previousY;
                                continue;
                            }
                            continue;
                        case'W':
                            previousX = currentX;
                            previousY = currentY;
                            if(currentX == 0)
                                currentX = planetX-1;
                            else
                                currentX--;
                            if(planetContainsObstacleAt(currentX, currentY)){
                            	if(!obstacles.contains("("+ Integer.toString(currentX)+","+Integer.toString(currentY)+")"))
                                    obstacles=obstacles+("("+ Integer.toString(currentX)+","+Integer.toString(currentY)+")");
                                currentX = previousX;
                                currentY = previousY;
                                continue;
                            }
                            continue;
                    }
                case 'b':
                    switch (dir){
                        case 'N':
                            previousX = currentX;
                            previousY = currentY;
                            if(currentY == 0)
                                currentY = planetY-1;
                            else
                                currentY--;
                            if(planetContainsObstacleAt(currentX, currentY)){
                            	if(!obstacles.contains("("+ Integer.toString(currentX)+","+Integer.toString(currentY)+")"))
                                    obstacles=obstacles+("("+ Integer.toString(currentX)+","+Integer.toString(currentY)+")");
                                currentX = previousX;
                                currentY = previousY;
                                continue;
                            }
                            continue;
                        case'S':
                            previousX = currentX;
                            previousY = currentY;
                            if(currentY == planetY-1)
                                currentY = 0;
                            else
                                currentY++;
                            if(planetContainsObstacleAt(currentX, currentY)){
                            	if(!obstacles.contains("("+ Integer.toString(currentX)+","+Integer.toString(currentY)+")"))
                                    obstacles=obstacles+("("+ Integer.toString(currentX)+","+Integer.toString(currentY)+")");
                                currentX = previousX;
                                currentY = previousY;
                                continue;
                            }
                            continue;
                        case'E':
                            previousX = currentX;
                            previousY = currentY;
                            if(currentX == 0)
                                currentX = planetX-1;
                            else
                                currentX--;
                            if(planetContainsObstacleAt(currentX, currentY)){
                            	if(!obstacles.contains("("+ Integer.toString(currentX)+","+Integer.toString(currentY)+")"))
                                    obstacles=obstacles+("("+ Integer.toString(currentX)+","+Integer.toString(currentY)+")");
                                currentX = previousX;
                                currentY = previousY;
                                continue;
                            }
                            continue;
                        case'W':
                            previousX = currentX;
                            previousY = currentY;
                            if(currentX == planetX-1)
                                currentX = 0;
                            else
                                currentX++;
                            if(planetContainsObstacleAt(currentX, currentY)){
                            	if(!obstacles.contains("("+ Integer.toString(currentX)+","+Integer.toString(currentY)+")"))
                                    obstacles=obstacles+("("+ Integer.toString(currentX)+","+Integer.toString(currentY)+")");
                                currentX = previousX;
                                currentY = previousY;
                                continue;
                            }
                            continue;
                    }
            }
        }
        return landingstatus = "(" + currentX + "," + currentY + "," + dir + ")" + obstacles;
    }

    
    
    public char turnLeft(char dir){
	        char newdir =' ';
	        if(dir=='N'){
	            newdir='W';
	        }
	        if(dir=='W'){
	            newdir='S';
	        }
	        if(dir=='S'){
	            newdir='E';
	        }
	        if(dir=='E'){
	            newdir='N';
	        }
	        return newdir;
	    }
	    public char turnRight(char dir){
	        char newdir =' ';
	        if(dir=='N'){
	            newdir='E';
	        }
	        if(dir=='E'){
	            newdir='S';
	        }
	        if(dir=='S'){
	            newdir='W';
	        }
	        if(dir=='W'){
	            newdir='N';
	        }
	        return newdir;

	    }
	   

}
	

